True Negatives
============

"True Negatives" or "Ocean clutter" - These are areas of the SAR imagery which a) was not a ship and b) not a false positive. These mostly correspond to ocean areas. Once again the number of true negative samples is in order of hundreds of millions and so only a small random sample of these were selected for the dataset.


JSON Structure Tree
============

- Example 1 
  - sensor
  - fullname
  - polarisation
  - incidenceangle
  - row
  - column
  - latitude
  - longitude
  - gttype
  - thresh
  - R-shape
      - LatitudeLimits
      -   LongitudeLimits
      - RasterSize
      - RasterInterpretation
      - ColumnStartFrom
      - RowsStartFrom
      - CellExtentInLatitude
      - CellExtentInLongitude
      - RasterExtentInLatitude
      - RasterExtentinLongitude
      - XIntrinsicLimits
      - YIntrinsicLimits
      - CoordinateSystemType
      - AngleUnit
  - patchfu
  - windowfu
 

JSON Attribute / Field description
============


| Attribute               | Description                                                                                                                                                                      |
|-------------------------|----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------|
| sensor                  | Sentinel 1 "SEN1" or RADARSAT-2 "RS2"                                                                                                                                            |
| fullname                | Full name of satellite image the false positive came from                                                                                                                        |
| polarisation            | One of four associated polarisations "HH", "HV", "VV", "VH"                                                                                                                      |
| incidenceangle          | Local incidence angle of the false positive's centre                                                                                                                             |
| row                     | Georeferenced image row number at centre                                                                                                                                         |
| col                     | Georeferenced image column number at centre                                                                                                                                      |
| latitude                | Latitude coordinate using WGS84 at centre                                                                                                                                        |
| longitude               | Longitude coordinate using WGS84 at centre                                                                                                                                       |
| gttype                  | Type of data: "FP" for false positive and "NEG" for true negative                                                                                                                |
| threshold               | CA-CFAR threshold used to identify false positive, 0.0 for true negatives and 1.0 for rest                                                                                       |
| R                       | Structure containing the georeferenced information of the entire SAR image                                                                                                       |
| LatitudeLimits          | [Min Max] of Latitude for the SAR image's entire extent                                                                                                                          |
| LongitudeLimits         | [Min Max] of Longitude for the SAR image's entire extent                                                                                                                         |
| RasterSize              | Georeference size in pixels                                                                                                                                                      |
| RasterInterpretation    | Raster edges control ("cells" or "postings")                                                                                                                                     |
| ColumnStartFrom         | Edge from which column indexing starts ("north" or "south")                                                                                                                      |
| RowStartFrom            | Edge from which row indexing starts ("east" or "west")                                                                                                                           |
| CellExtentInLatitude    | Height of cells                                                                                                                                                                  |
| CellExtentInLongitude   | Width of cells                                                                                                                                                                   |
| RasterExtentInLatitude  | Difference between [Min Max] Latitudes                                                                                                                                           |
| RasterExtentInLongitude | Difference between [Min Max] Longitudes                                                                                                                                          |
| XIntrinsicLimits        | Limits of image in intrinsic units in X dimension                                                                                                                                |
| YIntrinsicLimits        | Limits of image in intrinsic units in Y dimension                                                                                                                                |
| CoordinateSystemType    | Type of referencing ("planar" or "geographic")                                                                                                                                   |
| AngleUnit               | Type of unit for angle measurements ("degrees" or "radians")                                                                                                                     |
| patchfu                 | [51 51] radiometrically calibrated rectangle image centred on the false positive. May contain other false positives/lookalikes/ships                                             |
| windowfu                | [21 21] radiometrically calibrated rectangle image centred on the false positive. Does not contain other false positives/lookalikes/ships                                        |


Reference
------
Please ensure the following reference is used with this work:

    @inproceedings{SAR:Schwegmann2016, 
    author = {C. P. Schwegmann and W. Kleynhans and B. P. Salmon and L. W. Mdakane and R. G. V. Meyer}, 
    title = {{Very deep learning for false positive discrimination in Synthetic Aperture Radar imagery}}, 
    booktitle = {{IEEE International Geoscience and Remote Sensing Symposium (IGARSS)}}, 
    year = {2016}, 
    month = jul, 
    pages = {104--107}, 
    }

License
-------
SARmaps is an open source project licensed under [MIT](http://opensource.org/licenses/MIT).

