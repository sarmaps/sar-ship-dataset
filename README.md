Introduction
============

**SAR Dataset** -- This readme describes the SAR dataset used for SAR ship detection.
For a full technical description of the dataset see the accompaying file "dataset_paper.pdf".

This dataset was created to help facilitate meaningful comparisons between SAR ship detection and discrimination methods but also provides additional information about some of the ships within the dataset from matched AIS transmissions. These matched ships allow for additional research into information extraction from SAR medium resolution imagery. This distribution of the dataset consists of only the ships and not the raw SAR imagery.

The dataset is split into 3 components, described as:

"True Positives" or "Ships/Vessels" - These are the reverified ships contained within the 46 SAR images. The steps by which these were selected as actual ships is detailed in the included pdf file. For each ship there is an associated "ground truth" which is a pixel-by-pixel binary version of the ships. From the binary version the various attributes are calculated such as centroid coordinates, length/width and area. 

See file "ship_positives.md" for a description of the fields inside the "ship_positives.json" file.

"False Positives" or "Lookalikes" - These are areas of the SAR imagery which look like ships but for various reason were rejected (too small, larger area observation, ghosts of ships... etc.). These were identified by using a low threshold CFAR method and then removing all of the true positives from the list. Across the entire 46 SAR dataset the low threshold CFAR provided approximately 450 000 false positives and a random sampling of these have been included in this dataset. 

See file "false_positives.md" for a description of the fields inside the "false_positives.json" file.

"True Negatives" or "Ocean clutter" - These are areas of the SAR imagery which a) was not a ship and b) not a false positive. These mostly correspond to ocean areas. Once again the number of true negative samples is in order of hundreds of millions and so only a small random sample of these were selected for the dataset.  

See file "true_negatives.md" for a description of the fields inside the "true_negatives.json" file.

Finally, the file "sar_ship_dataset.mat" is provided for convenience in working with the data in MATLAB. 

Reference
------
Please ensure the following reference is used with this work:

    @inproceedings{SAR:Schwegmann2016, 
    author = {C. P. Schwegmann and W. Kleynhans and B. P. Salmon and L. W. Mdakane and R. G. V. Meyer}, 
    title = {{Very deep learning for ship discrimination in Synthetic Aperture Radar imagery}}, 
    booktitle = {{IEEE International Geoscience and Remote Sensing Symposium (IGARSS)}}, 
    year = {2016}, 
    month = jul, 
    pages = {104--107}, 
    }

License
-------
The SAR Ship dataset is an open source project licensed under [MIT](http://opensource.org/licenses/MIT).

