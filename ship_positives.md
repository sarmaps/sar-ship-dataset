True Positive
============

"True Positives" or "Ships/Vessels" - These are the reverified ships contained within the 46 SAR images. The steps by which these were selected as actual ships is detailed in the included pdf file. For each ship there is an associated "ground truth" which is a pixel-by-pixel binary version of the ships. From the binary version the various attributes are calculated such as centroid coordinates, length/width and area. 

JSON Structure Tree
============

- Ship 1 
  - id
  - sensor
  - fullname
  - polarisation
  - incidenceangle
  - row
  - col
  - latitude
  - longitude
  - stats
    - area
    - centroid
        - boundingbox
        - subarrayidx
        - MajorAxisLength
        - MinorAxisLength
        - Eccentricity
        - Orientation
        - ConvexHull
        - ConvexImage
        - ConvexArea
        - Image
        - FilledImage
        - EulerNumber
        - Extrema
        - EquivDiameter
        - Solidity
        - Extent
        - PixelIdxList
        - PixelList
        - Perimeter
        - PerimeterOld
        - OrientationRadon
   - R-shape
     - LatitudeLimits
     - LongitudeLimits
     - RasterSize
     - RasterInterpretation
     - ColumnStartFrom
     - RowsStartFrom
     - CellExtentInLatitude
     - CellExtentInLongitude
     - RasterExtentInLatitude
     - RasterExtentinLongitude
     - XIntrinsicLimits
     - YIntrinsicLimits
     - CoordinateSystemType
     - AngleUnit
   - Nearest
     - dist
     - idxthis
     - idxthat
     - type
     - typetext
   - pixelsize
   - patchfu
   - patchgt
   - windowfu
   - windowgt
   - validais
   - ais
     - imo
     - mmsi
     - dbcommsid
     - name
     - callsign
     - flag
     - bearing
     - length
     - breadth
     - type
     - sog
     - grosstonnage
     - deadweight
     - year
     - aismindist
     - trackmindist
   - aux
     - mthead
     - mtline
     - aishead
     - aisline
     - hardmatch
     - softmatch
     - estlon
     - estlat
     - saraistimed
     - trackpoint
     - timedelta
     - nsx
     - mtdraught
     - mtstatus


JSON Attribute / Field description
============


| Attribute               | Description                                                                                                                                                                      |
|-------------------------|----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------|
| id                      | Unique string ID of the specific ship. Created in the format 'SENSOR_IMAGEID_POL_NUMBER' eg ("S1A_00520E_HH_0001")                                                               |
| sensor                  | Sentinel 1 "SEN1" or RADARSAT-2 "RS2"                                                                                                                                            |
| fullname                | Full name of satellite image the ship came from                                                                                                                                  |
| polarisation            | One of four associated polarisations "HH", "HV", "VV", "VH"                                                                                                                      |
| incidenceangle          | Local incidence angle of the ship's centroid.                                                                                                                                    |
| row                     | Georeferenced image row number                                                                                                                                                   |
| col                     | Georeferenced image column number                                                                                                                                                |
| latitude                | Latitude coordinate using WGS84                                                                                                                                                  |
| longitude               | Longitude coordinate using WGS84                                                                                                                                                 |
| stats                   | Structure containing statistical image information of the ship from the ground truth image. See MATLAB's "regionprops" for a detailed explanation of each of the stats variables |
| area                    | Pixel area of ship                                                                                                                                                               |
| centroid                | Relative centroid position within the Patch image                                                                                                                                |
| boundingbox             | Smallest rectangle containing the ship                                                                                                                                           |
| subarrayidx             | Indices to extract elements of ship within bounding box                                                                                                                          |
| MajorAxisLength         | Number of pixels of the major axis of an enclosing ellipse                                                                                                                       |
| MinorAxisLength         | Number of pixels of the major axis of an enclosing ellipse                                                                                                                       |
| Eccentricity            | Specifies the eccentricity of the enclosing ellipse                                                                                                                              |
| Orientation             | Angle in degrees between -90 and 90                                                                                                                                              |
| ConvexHull              | Smallest convext polygon coordinates                                                                                                                                             |
| ConvexImage             | Binary image describing convex hull                                                                                                                                              |
| ConvexArea              | Area in pixels of the convex hull                                                                                                                                                |
| Image                   | Binary image of the bounding box                                                                                                                                                 |
| FilledImage             | Binary image of the bounding box without holes                                                                                                                                   |
| EulerNumber             | Scalar value specifying number of objects minus number of holes in objects                                                                                                       |
| Extrema                 | Coordinates of the objects extrema as [top-left top-right right-top right-bottom bottom-right bottom-left left-bottom left-top]                                                  |
| EquivDiameter           | Scalar representing the diameter of a circle with the same area as the region                                                                                                    |
| Solidity                | Scalar representing the proportion of pixels in the convex hull also in the region                                                                                               |
| Extent                  | Ratio of pixels in region to the total number of pixels in bounding box                                                                                                          |
| PixelIdxList            | Linear pixel indices for region within the Patch                                                                                                                                 |
| PixelList               | Row-Col indices for region within the Patch                                                                                                                                      |
| Perimeter               | Perimeter in pixels                                                                                                                                                              |
| PerimeterOld            | Older rougher estimation of perimeter in pixels                                                                                                                                  |
| OrientationRadon        | Orientation callculated using the Radon transform between 0 - 180 degrees (due to ambiguties).                                                                                   |
| R                       | Structure containing the georeferenced information of the entire SAR image                                                                                                       |
| LatitudeLimits          | [Min Max] of Latitude for the SAR image's entire extent                                                                                                                          |
| LongitudeLimits         | [Min Max] of Longitude for the SAR image's entire extent                                                                                                                         |
| RasterSize              | Georeference size in pixels                                                                                                                                                      |
| RasterInterpretation    | Raster edges control ("cells" or "postings")                                                                                                                                     |
| ColumnStartFrom         | Edge from which column indexing starts ("north" or "south")                                                                                                                      |
| RowStartFrom            | Edge from which row indexing starts ("east" or "west")                                                                                                                           |
| CellExtentInLatitude    | Height of cells                                                                                                                                                                  |
| CellExtentInLongitude   | Width of cells                                                                                                                                                                   |
| RasterExtentInLatitude  | Difference between [Min Max] Latitudes                                                                                                                                           |
| RasterExtentInLongitude | Difference between [Min Max] Longitudes                                                                                                                                          |
| XIntrinsicLimits        | Limits of image in intrinsic units in X dimension                                                                                                                                |
| YIntrinsicLimits        | Limits of image in intrinsic units in Y dimension                                                                                                                                |
| CoordinateSystemType    | Type of referencing ("planar" or "geographic")                                                                                                                                   |
| AngleUnit               | Type of unit for angle measurements ("degrees" or "radians")                                                                                                                     |
| Nearest                 | Structure detailing the nearest ship to the current one                                                                                                                          |
| dist                    | Distance in pixels to the nearest ship                                                                                                                                           |
| idxthis                 | The index of this ship in dataset                                                                                                                                                |
| idxthat                 | The index of the nearest ship in the dataset                                                                                                                                     |
| type                    | Type of nearest match (1-6)                                                                                                                                                      |
| typetype                | Text description of the type of match. Describes a) Same/different acquisition, b) Same/different polarisation and c) Same/different ship                                        |
| pixelsize               | The average size of a single pixel of the form [range azimuth] (in meters)                                                                                                       |
| patchfu                 | [101 101] radiometrically calibrated rectangle image centred on the ship. May contain other ships/lookalikes                                                                     |
| patchgt                 | [101 101] binary rectangle image centred on the ship. Only the ship pixels are set to 1                                                                                          |
| windowfu                | [21 21] radiometrically calibrated rectangle image centred on the ship. Does not contain other ships/lookalikes                                                                  |
| windowgt                | [21 21] binary rectangle image centred on the ship. Only the ship pixels are set to 1                                                                                            |
| validais                | Binary field indicating if this ship has a valid ais data (1) or not (0)                                                                                                         |
| ais                     | Structure containing AIS information if validais == 1                                                                                                                            |
| imo                     | International Maritime Organisation Number                                                                                                                                       |
| mmsi                    | Maritime Mobile Security Identifier                                                                                                                                              |
| dbcommsid               | Communications ID                                                                                                                                                                |
| name                    | Ship name                                                                                                                                                                        |
| callsign                | Ship radio callsign                                                                                                                                                              |
| flag                    | Ship's registered flag                                                                                                                                                           |
| bearing                 | The ships bearing (orientation) in degrees at matched message                                                                                                                    |
| length                  | Ship's reported length                                                                                                                                                           |
| breadth                 | Ship's reported breadth                                                                                                                                                          |
| type                    | Ship type                                                                                                                                                                        |
| sog                     | Speed over ground                                                                                                                                                                |
| grosstonnage            | Ship's reported gross tonnage                                                                                                                                                    |
| deadweight              | Ship's reported deadweight                                                                                                                                                       |
| year                    | Year of manufacture                                                                                                                                                              |
| aismindist              | The distance between the ship centroid and its nearest match AIS message in meters                                                                                               |
| trackmindist            | The distance between the ship centroid and its nearest AIS track in meters                                                                                                       |
| aux                     | Structure containing auxillary information about the match                                                                                                                       |
| mthead                  | Headers for information from marinetraffic.com                                                                                                                                   |
| mtline                  | Ship information from marinetraffic.com                                                                                                                                          |
| aishead                 | AIS information headers                                                                                                                                                          |
| aisline                 | AIS information line data                                                                                                                                                        |
| hardmatch               | Binary indicating if match is hard (1) or soft (0)                                                                                                                               |
| softmatch               | Binary indicating if match is soft (1) or hard (0)                                                                                                                               |
| estlon                  | Estimated Longitude at time of acquisition                                                                                                                                       |
| estlat                  | Estimated Latitude at time of acquisition                                                                                                                                        |
| saraistimed             | Difference between SAR ship acquition time and AIS message time in seconds                                                                                                       |
| trackpoint              | Point of matched position in POINT(Lon,Lat) format                                                                                                                               |
| timedelta               | Difference between matched AIS message/track and SAR ship acquisition time                                                                                                       |
| nsx                     | North south matching                                                                                                                                                             |
| mtdraught               | Draught information from marinetraffic.com                                                                                                                                       |
| mtstatus                | Status information from marinetraffic.com                                                                                                                                        |




Reference
      
Please ensure the following reference is used with this work:

    @inproceedings{SAR:Schwegmann2016, 
    author = {C. P. Schwegmann and W. Kleynhans and B. P. Salmon and L. W. Mdakane and R. G. V. Meyer}, 
    title = {{Very deep learning for ship discrimination in Synthetic Aperture Radar imagery}}, 
    booktitle = {{IEEE International Geoscience and Remote Sensing Symposium (IGARSS)}}, 
    year = {2016}, 
    month = jul, 
    pages = {104  107}, 
    }

License
      -
SARmaps is an open source project licensed under [MIT](http://opensource.org/licenses/MIT).

